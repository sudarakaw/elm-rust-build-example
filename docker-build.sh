#!/usr/bin/env sh

docker run \
  --rm \
  -v .:/app \
  app-builder \
  /usr/local/cargo/bin/cargo tauri build

docker run \
  --rm \
  -v .:/app \
  app-builder \
  /bin/chown -R 1000:1000 .

