use std::{env, fs, path::Path, process::Command};

fn main() {
    let src = Path::new("src/Counter.elm");
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest = Path::new(&out_dir).join("app.js");

    println!("cargo:rerun-if-changed={}", src.to_str().unwrap());

    let mut command = Command::new("elm");

    command
        .arg("make")
        .arg(format!("--output={}", dest.to_str().unwrap()))
        .arg(src.to_str().unwrap());

    #[cfg(debug_assertions)]
    command.arg("--debug");

    #[cfg(not(debug_assertions))]
    command.arg("--optimize");

    let output = command.output().unwrap();

    if !output.status.success() {
        println!("{}", String::from_utf8_lossy(&output.stderr));

        std::process::exit(1);
    }

    #[cfg(not(debug_assertions))]
    fs::copy(dest, "assets/app.js").unwrap();
}
