module Counter exposing (main)

import Browser
import Html exposing (Html, button, div, h1, main_, p, span, text)
import Html.Attributes exposing (class, style, type_)
import Html.Events exposing (onClick)



-- MODEL


type alias Model =
    { count : Int
    , message : Maybe ( String, String )
    , broken : Bool
    }



-- UPDATE


type Msg
    = Inc
    | Dec


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    if model.broken then
        ( model, Cmd.none )

    else
        case msg of
            Inc ->
                ( { model | count = model.count + 1 } |> validate, Cmd.none )

            Dec ->
                ( { model | count = model.count - 1 } |> validate, Cmd.none )


validate : Model -> Model
validate model =
    if 0 > model.count then
        { model | count = 0, message = Just ( "light", "Let's not go there" ) }

    else if 7 < model.count && 10 > model.count then
        { model | message = Just ( "info", "Keep it under ten" ) }

    else if 10 == model.count then
        { model | message = Just ( "warning", "NO!" ) }

    else if 10 < model.count then
        { model | message = Just ( "danger", "Oh grate! now you broke it" ), broken = True }

    else
        { model | message = Nothing }



-- VIEW


view : Model -> Html Msg
view { count, message } =
    main_ [ class "container", style "height" "calc(100vh - 2rem)" ]
        [ div [ class "bg-light p-5 rounded mt-3 text-center", style "height" "100%" ]
            [ h1 [] [ text "Let's Count!" ]
            , button [ type_ "button", class "btn btn-lg btn-primary fs-2", onClick Dec ] [ text "-" ]
            , span [ class "fs-1 mx-3" ] [ count |> String.fromInt |> text ]
            , button [ type_ "button", class "btn btn-lg btn-primary fs-2", onClick Inc ] [ text "+" ]
            , case message of
                Just ( alert_type, msg ) ->
                    p [ class ("mt-4 alert alert-" ++ alert_type) ] [ text msg ]

                Nothing ->
                    text ""
            ]
        ]



-- MAIN


init : () -> ( Model, Cmd Msg )
init () =
    ( { count = 0, message = Nothing, broken = False }, Cmd.none )


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }
