FROM rust:slim-buster

## System update
RUN apt-get update && \
    apt-get upgrade -y

# Install cUrl to grab prebuild binary compilers & build tools
RUN apt-get install -y curl

# Download Elm compiler
RUN curl -L -o elm.gz https://github.com/elm/compiler/releases/download/0.19.1/binary-for-linux-64-bit.gz \
    && gunzip elm.gz \
    && chmod +x elm \
    && mv elm /usr/local/bin

# Download Tauri builder
RUN curl -L -o tauri.tgz https://github.com/tauri-apps/tauri/releases/download/cli.rs-v1.2.3/cargo-tauri-x86_64-unknown-linux-gnu.tgz \
        && tar -xf tauri.tgz cargo-tauri \
        && chmod +x cargo-tauri \
        && mv cargo-tauri /usr/local/bin

# Install system packages required to build Tauri app
RUN apt-get install -y pkg-config libwebkit2gtk-4.0-dev

# Clean up
RUN apt-get remove -y curl \
    && apt-get autoremove -y

WORKDIR /app
