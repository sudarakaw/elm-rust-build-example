use std::convert::Infallible;

use hyper::{
    http::HeaderValue,
    service::{make_service_fn, service_fn},
    Body, Request, Response, Server, StatusCode,
};

async fn hello(req: Request<Body>) -> Result<Response<Body>, Infallible> {
    match req.uri().path() {
        "/" => {
            let mut response = Response::new(include_str!("../assets/index.html").into());

            response
                .headers_mut()
                .insert("content-type", HeaderValue::from_str("text/html").unwrap());

            Ok(response)
        }
        "/init.js" => {
            let mut response = Response::new(include_str!("../assets/init.js").into());

            response.headers_mut().insert(
                "content-type",
                HeaderValue::from_str("text/javascript").unwrap(),
            );

            Ok(response)
        }
        "/bootstrap.css" => {
            let mut response = Response::new(include_str!("../assets/bootstrap.css").into());

            response
                .headers_mut()
                .insert("content-type", HeaderValue::from_str("text/css").unwrap());

            Ok(response)
        }
        "/app.js" => {
            let mut response =
                Response::new(include_str!(concat!(env!("OUT_DIR"), "/app.js")).into());

            response.headers_mut().insert(
                "content-type",
                HeaderValue::from_str("text/javascript").unwrap(),
            );

            Ok(response)
        }
        _ => {
            let mut not_found = Response::new("".into());

            *not_found.status_mut() = StatusCode::NOT_FOUND;

            Ok(not_found)
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), hyper::Error> {
    let addr = ([127, 0, 0, 1], 5000).into();

    let service = make_service_fn(|_conn| async { Ok::<_, Infallible>(service_fn(hello)) });

    let server = Server::bind(&addr).serve(service);

    server.await?;

    Ok(())
}
